/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"

namespace OPENSCENARIO
{
class CatalogBase : public Node
{
  public:
    using Vehicles_t = std::vector<std::unique_ptr<Node>>;
    using Controllers_t = std::vector<std::unique_ptr<Node>>;
    using Pedestrians_t = std::vector<std::unique_ptr<Node>>;
    using MiscObjects_t = std::vector<std::unique_ptr<Node>>;
    using Environments_t = std::vector<std::unique_ptr<Node>>;
    using Maneuvers_t = std::vector<std::unique_ptr<Node>>;
    using Trajectories_t = std::vector<std::unique_ptr<Node>>;
    using Routes_t = std::vector<std::unique_ptr<Node>>;

    explicit CatalogBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICatalog> catalog);
    CatalogBase(CatalogBase&&) = default;
    ~CatalogBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    Vehicles_t vehicles_;
    Controllers_t controllers_;
    Pedestrians_t pedestrians_;
    MiscObjects_t miscObjects_;
    Environments_t environments_;
    Maneuvers_t maneuvers_;
    Trajectories_t trajectories_;
    Routes_t routes_;
};

}  // namespace OPENSCENARIO

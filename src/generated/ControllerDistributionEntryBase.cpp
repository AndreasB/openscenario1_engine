/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerDistributionEntryBase.h"

#include "CatalogReference.h"
#include "Controller.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static ControllerDistributionEntryBase::ControllerDistributionEntry_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControllerDistributionEntry> controllerDistributionEntry)
{
    if (auto element = controllerDistributionEntry->GetController(); element)
    {
        return Builder::transform<Controller>(element);
    }
    if (auto element = controllerDistributionEntry->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IControllerDistributionEntry");
}

ControllerDistributionEntryBase::ControllerDistributionEntryBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControllerDistributionEntry> controllerDistributionEntry)
    : Node{"ControllerDistributionEntryBase"}, controllerDistributionEntry_{(std::cout << "ControllerDistributionEntryBase instantiating controllerDistributionEntry_" << std::endl, resolve_choices(controllerDistributionEntry))}

{
}

bool ControllerDistributionEntryBase::Complete() const
{
    std::cout << "ControllerDistributionEntryBase complete?\n";
    return OPENSCENARIO::Complete(controllerDistributionEntry_);
}

void ControllerDistributionEntryBase::Step()
{
    std::cout << "ControllerDistributionEntryBase step!\n";
    OPENSCENARIO::Step(controllerDistributionEntry_);
}

void ControllerDistributionEntryBase::Stop()
{
    std::cout << "ControllerDistributionEntryBase stop!\n";
    OPENSCENARIO::Stop(controllerDistributionEntry_);
}

}  // namespace OPENSCENARIO

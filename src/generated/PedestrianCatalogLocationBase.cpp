/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PedestrianCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
PedestrianCatalogLocationBase::PedestrianCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPedestrianCatalogLocation> pedestrianCatalogLocation)
    : Node{"PedestrianCatalogLocationBase"}, directory_{(std::cout << "PedestrianCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(pedestrianCatalogLocation->GetDirectory()))}

{
}

bool PedestrianCatalogLocationBase::Complete() const
{
    std::cout << "PedestrianCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void PedestrianCatalogLocationBase::Step()
{
    std::cout << "PedestrianCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void PedestrianCatalogLocationBase::Stop()
{
    std::cout << "PedestrianCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO

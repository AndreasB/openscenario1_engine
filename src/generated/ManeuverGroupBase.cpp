/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ManeuverGroupBase.h"

#include "Actors.h"
#include "CatalogReference.h"
#include "Maneuver.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ManeuverGroupBase::ManeuverGroupBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IManeuverGroup> maneuverGroup)
    : Node{"ManeuverGroupBase"}, actors_{(std::cout << "ManeuverGroupBase instantiating actors_" << std::endl, Builder::transform<Actors>(maneuverGroup->GetActors()))}, catalogReferences_{(std::cout << "ManeuverGroupBase instantiating catalogReferences_" << std::endl, Builder::transform<CatalogReference, CatalogReferences_t>(maneuverGroup, &NET_ASAM_OPENSCENARIO::v1_0::IManeuverGroup::GetCatalogReferences))}, maneuvers_{(std::cout << "ManeuverGroupBase instantiating maneuvers_" << std::endl, Builder::transform<Maneuver, Maneuvers_t>(maneuverGroup, &NET_ASAM_OPENSCENARIO::v1_0::IManeuverGroup::GetManeuvers))}

{
}

bool ManeuverGroupBase::Complete() const
{
    std::cout << "ManeuverGroupBase complete?\n";
    return OPENSCENARIO::Complete(actors_) && OPENSCENARIO::Complete(catalogReferences_) && OPENSCENARIO::Complete(maneuvers_);
}

void ManeuverGroupBase::Step()
{
    std::cout << "ManeuverGroupBase step!\n";
    OPENSCENARIO::Step(actors_);
    OPENSCENARIO::Step(catalogReferences_);
    OPENSCENARIO::Step(maneuvers_);
}

void ManeuverGroupBase::Stop()
{
    std::cout << "ManeuverGroupBase stop!\n";
    OPENSCENARIO::Stop(actors_);
    OPENSCENARIO::Stop(catalogReferences_);
    OPENSCENARIO::Stop(maneuvers_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SpeedActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "SpeedActionTarget.h"
#include "TransitionDynamics.h"

namespace OPENSCENARIO
{
SpeedActionBase::SpeedActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISpeedAction> speedAction)
    : Node{"SpeedActionBase"}, speedActionDynamics_{(std::cout << "SpeedActionBase instantiating speedActionDynamics_" << std::endl, Builder::transform<TransitionDynamics>(speedAction->GetSpeedActionDynamics()))}, speedActionTarget_{(std::cout << "SpeedActionBase instantiating speedActionTarget_" << std::endl, Builder::transform<SpeedActionTarget>(speedAction->GetSpeedActionTarget()))}

{
}

bool SpeedActionBase::Complete() const
{
    std::cout << "SpeedActionBase complete?\n";
    return OPENSCENARIO::Complete(speedActionDynamics_) && OPENSCENARIO::Complete(speedActionTarget_);
}

void SpeedActionBase::Step()
{
    std::cout << "SpeedActionBase step!\n";
    OPENSCENARIO::Step(speedActionDynamics_);
    OPENSCENARIO::Step(speedActionTarget_);
}

void SpeedActionBase::Stop()
{
    std::cout << "SpeedActionBase stop!\n";
    OPENSCENARIO::Stop(speedActionDynamics_);
    OPENSCENARIO::Stop(speedActionTarget_);
}

}  // namespace OPENSCENARIO

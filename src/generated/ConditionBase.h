/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/ConditionEdge.h"
#include "Common/Node.h"
#include "MantleAPI/Execution/i_environment.h"

namespace OPENSCENARIO
{
class ConditionBase : public Node
{
  public:
    using Condition_t = std::unique_ptr<Node>;

    explicit ConditionBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICondition> condition,
                           mantle_api::IEnvironment& environment);
    ConditionBase(ConditionBase&&) = default;
    ~ConditionBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    mantle_api::IEnvironment& environment_;
    std::unique_ptr<EdgeEvaluatorBase> conditionEdge_;
    Condition_t condition_;
};

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AcquirePositionActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
AcquirePositionActionBase::AcquirePositionActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAcquirePositionAction> acquirePositionAction)
    : Node{"AcquirePositionActionBase"}, position_{(std::cout << "AcquirePositionActionBase instantiating position_" << std::endl, Builder::transform<Position>(acquirePositionAction->GetPosition()))}

{
}

bool AcquirePositionActionBase::Complete() const
{
    std::cout << "AcquirePositionActionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void AcquirePositionActionBase::Step()
{
    std::cout << "AcquirePositionActionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void AcquirePositionActionBase::Stop()
{
    std::cout << "AcquirePositionActionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO

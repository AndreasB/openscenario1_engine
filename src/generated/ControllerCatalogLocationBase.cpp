/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ControllerCatalogLocationBase::ControllerCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControllerCatalogLocation> controllerCatalogLocation)
    : Node{"ControllerCatalogLocationBase"}, directory_{(std::cout << "ControllerCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(controllerCatalogLocation->GetDirectory()))}

{
}

bool ControllerCatalogLocationBase::Complete() const
{
    std::cout << "ControllerCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void ControllerCatalogLocationBase::Step()
{
    std::cout << "ControllerCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void ControllerCatalogLocationBase::Stop()
{
    std::cout << "ControllerCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO

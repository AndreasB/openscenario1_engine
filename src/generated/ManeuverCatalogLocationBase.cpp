/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ManeuverCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ManeuverCatalogLocationBase::ManeuverCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IManeuverCatalogLocation> maneuverCatalogLocation)
    : Node{"ManeuverCatalogLocationBase"}, directory_{(std::cout << "ManeuverCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(maneuverCatalogLocation->GetDirectory()))}

{
}

bool ManeuverCatalogLocationBase::Complete() const
{
    std::cout << "ManeuverCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void ManeuverCatalogLocationBase::Step()
{
    std::cout << "ManeuverCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void ManeuverCatalogLocationBase::Stop()
{
    std::cout << "ManeuverCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSourceActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"
#include "TrafficDefinition.h"

namespace OPENSCENARIO
{
TrafficSourceActionBase::TrafficSourceActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSourceAction> trafficSourceAction)
    : Node{"TrafficSourceActionBase"}, position_{(std::cout << "TrafficSourceActionBase instantiating position_" << std::endl, Builder::transform<Position>(trafficSourceAction->GetPosition()))}, trafficDefinition_{(std::cout << "TrafficSourceActionBase instantiating trafficDefinition_" << std::endl, Builder::transform<TrafficDefinition>(trafficSourceAction->GetTrafficDefinition()))}

{
}

bool TrafficSourceActionBase::Complete() const
{
    std::cout << "TrafficSourceActionBase complete?\n";
    return OPENSCENARIO::Complete(position_) && OPENSCENARIO::Complete(trafficDefinition_);
}

void TrafficSourceActionBase::Step()
{
    std::cout << "TrafficSourceActionBase step!\n";
    OPENSCENARIO::Step(position_);
    OPENSCENARIO::Step(trafficDefinition_);
}

void TrafficSourceActionBase::Stop()
{
    std::cout << "TrafficSourceActionBase stop!\n";
    OPENSCENARIO::Stop(position_);
    OPENSCENARIO::Stop(trafficDefinition_);
}

}  // namespace OPENSCENARIO

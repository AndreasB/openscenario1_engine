/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "FinalSpeedBase.h"

#include "AbsoluteSpeed.h"
#include "OpenScenarioEngineFactory.h"
#include "RelativeSpeedToMaster.h"

namespace OPENSCENARIO
{
static FinalSpeedBase::FinalSpeed_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IFinalSpeed> finalSpeed)
{
    if (auto element = finalSpeed->GetAbsoluteSpeed(); element)
    {
        return Builder::transform<AbsoluteSpeed>(element);
    }
    if (auto element = finalSpeed->GetRelativeSpeedToMaster(); element)
    {
        return Builder::transform<RelativeSpeedToMaster>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IFinalSpeed");
}

FinalSpeedBase::FinalSpeedBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IFinalSpeed> finalSpeed)
    : Node{"FinalSpeedBase"}, finalSpeed_{(std::cout << "FinalSpeedBase instantiating finalSpeed_" << std::endl, resolve_choices(finalSpeed))}

{
}

bool FinalSpeedBase::Complete() const
{
    std::cout << "FinalSpeedBase complete?\n";
    return OPENSCENARIO::Complete(finalSpeed_);
}

void FinalSpeedBase::Step()
{
    std::cout << "FinalSpeedBase step!\n";
    OPENSCENARIO::Step(finalSpeed_);
}

void FinalSpeedBase::Stop()
{
    std::cout << "FinalSpeedBase stop!\n";
    OPENSCENARIO::Stop(finalSpeed_);
}

}  // namespace OPENSCENARIO

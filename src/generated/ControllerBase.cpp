/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Properties.h"

namespace OPENSCENARIO
{
ControllerBase::ControllerBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IController> controller)
    : Node{"ControllerBase"}, properties_{(std::cout << "ControllerBase instantiating properties_" << std::endl, Builder::transform<Properties>(controller->GetProperties()))}, parameterDeclarations_{(std::cout << "ControllerBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(controller, &NET_ASAM_OPENSCENARIO::v1_0::IController::GetParameterDeclarations))}

{
}

bool ControllerBase::Complete() const
{
    std::cout << "ControllerBase complete?\n";
    return OPENSCENARIO::Complete(properties_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void ControllerBase::Step()
{
    std::cout << "ControllerBase step!\n";
    OPENSCENARIO::Step(properties_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void ControllerBase::Stop()
{
    std::cout << "ControllerBase stop!\n";
    OPENSCENARIO::Stop(properties_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO

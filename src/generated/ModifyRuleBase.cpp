/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ModifyRuleBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterAddValueRule.h"
#include "ParameterMultiplyByValueRule.h"

namespace OPENSCENARIO
{
static ModifyRuleBase::ModifyRule_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IModifyRule> modifyRule)
{
    if (auto element = modifyRule->GetAddValue(); element)
    {
        return Builder::transform<ParameterAddValueRule>(element);
    }
    if (auto element = modifyRule->GetMultiplyByValue(); element)
    {
        return Builder::transform<ParameterMultiplyByValueRule>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IModifyRule");
}

ModifyRuleBase::ModifyRuleBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IModifyRule> modifyRule)
    : Node{"ModifyRuleBase"}, modifyRule_{(std::cout << "ModifyRuleBase instantiating modifyRule_" << std::endl, resolve_choices(modifyRule))}

{
}

bool ModifyRuleBase::Complete() const
{
    std::cout << "ModifyRuleBase complete?\n";
    return OPENSCENARIO::Complete(modifyRule_);
}

void ModifyRuleBase::Step()
{
    std::cout << "ModifyRuleBase step!\n";
    OPENSCENARIO::Step(modifyRule_);
}

void ModifyRuleBase::Stop()
{
    std::cout << "ModifyRuleBase stop!\n";
    OPENSCENARIO::Stop(modifyRule_);
}

}  // namespace OPENSCENARIO

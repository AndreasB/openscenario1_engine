/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TimeReferenceBase.h"

#include "None.h"
#include "OpenScenarioEngineFactory.h"
#include "Timing.h"

namespace OPENSCENARIO
{
static TimeReferenceBase::TimeReference_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeReference> timeReference)
{
    if (auto element = timeReference->GetNone(); element)
    {
        return Builder::transform<None>(element);
    }
    if (auto element = timeReference->GetTiming(); element)
    {
        return Builder::transform<Timing>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ITimeReference");
}

TimeReferenceBase::TimeReferenceBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeReference> timeReference)
    : Node{"TimeReferenceBase"}, timeReference_{(std::cout << "TimeReferenceBase instantiating timeReference_" << std::endl, resolve_choices(timeReference))}

{
}

bool TimeReferenceBase::Complete() const
{
    std::cout << "TimeReferenceBase complete?\n";
    return OPENSCENARIO::Complete(timeReference_);
}

void TimeReferenceBase::Step()
{
    std::cout << "TimeReferenceBase step!\n";
    OPENSCENARIO::Step(timeReference_);
}

void TimeReferenceBase::Stop()
{
    std::cout << "TimeReferenceBase stop!\n";
    OPENSCENARIO::Stop(timeReference_);
}

}  // namespace OPENSCENARIO

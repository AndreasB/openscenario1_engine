/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "InfrastructureAction.h"

namespace OPENSCENARIO
{

bool InfrastructureAction::Complete() const
{
    std::cout << "InfrastructureAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void InfrastructureAction::Step()
{
    std::cout << "InfrastructureAction::Step() not implemented yet. Doing nothing.\n";
}

void InfrastructureAction::Stop()
{
    std::cout << "InfrastructureAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
#include "ConditionGroup.h"

#include "Condition.h"

bool OPENSCENARIO::ConditionGroup::IsSatisfied() const
{
    std::cout << "ConditionGroup::IsSatisfied?\n";
    return std::all_of(conditions_.begin(), conditions_.end(), [](auto& c) { return c->IsSatisfied(); });
}
/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideSteeringWheelAction.h"

namespace OPENSCENARIO
{

bool OverrideSteeringWheelAction::Complete() const
{
    std::cout << "OverrideSteeringWheelAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideSteeringWheelAction::Step()
{
    std::cout << "OverrideSteeringWheelAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideSteeringWheelAction::Stop()
{
    std::cout << "OverrideSteeringWheelAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
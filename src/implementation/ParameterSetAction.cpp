/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ParameterSetAction.h"

namespace OPENSCENARIO
{

bool ParameterSetAction::Complete() const
{
    std::cout << "ParameterSetAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void ParameterSetAction::Step()
{
    std::cout << "ParameterSetAction::Step() not implemented yet. Doing nothing.\n";
}

void ParameterSetAction::Stop()
{
    std::cout << "ParameterSetAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/TimeHeadwayConditionBase.h"

namespace OPENSCENARIO
{
class TimeHeadwayCondition : public TimeHeadwayConditionBase
{
  public:
    explicit TimeHeadwayCondition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeHeadwayCondition> parsedTimeHeadwayCondition,
        mantle_api::IEnvironment& environment)
        : TimeHeadwayConditionBase(parsedTimeHeadwayCondition, environment)
    {
    }

    bool IsSatisfied() const override;
};

}  // namespace OPENSCENARIO

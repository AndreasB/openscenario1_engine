/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ParameterAction.h"

namespace OPENSCENARIO
{

bool ParameterAction::Complete() const
{
    std::cout << "ParameterAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void ParameterAction::Step()
{
    std::cout << "ParameterAction::Step() not implemented yet. Doing nothing.\n";
}

void ParameterAction::Stop()
{
    std::cout << "ParameterAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
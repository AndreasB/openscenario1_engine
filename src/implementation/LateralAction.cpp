/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LateralAction.h"

namespace OPENSCENARIO
{

bool LateralAction::Complete() const
{
    std::cout << "LateralAction::Complete() not implemented yet. Returning false.\n";
    return false;
}

void LateralAction::Step()
{
    std::cout << "LateralAction::Step() not implemented yet. Doing nothing.\n";
}

void LateralAction::Stop()
{
    std::cout << "LateralAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
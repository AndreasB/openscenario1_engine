/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideParkingBrakeAction.h"

namespace OPENSCENARIO
{

bool OverrideParkingBrakeAction::Complete() const
{
    std::cout << "OverrideParkingBrakeAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideParkingBrakeAction::Step()
{
    std::cout << "OverrideParkingBrakeAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideParkingBrakeAction::Stop()
{
    std::cout << "OverrideParkingBrakeAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
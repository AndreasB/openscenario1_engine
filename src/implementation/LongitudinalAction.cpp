/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalAction.h"

namespace OPENSCENARIO
{
bool LongitudinalAction::Complete() const
{
    return longitudinalAction_->Complete();
}

void LongitudinalAction::Step()
{
    longitudinalAction_->Step();
}

void LongitudinalAction::Stop()
{
    longitudinalAction_->Stop();
}

}  // namespace OPENSCENARIO
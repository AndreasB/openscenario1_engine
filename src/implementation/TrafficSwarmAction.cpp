/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSwarmAction.h"

namespace OPENSCENARIO
{

bool TrafficSwarmAction::Complete() const
{
    std::cout << "TrafficSwarmAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSwarmAction::Step()
{
    std::cout << "TrafficSwarmAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSwarmAction::Stop()
{
    std::cout << "TrafficSwarmAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
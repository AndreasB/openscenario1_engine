/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TeleportAction.h"

namespace OPENSCENARIO
{

bool TeleportAction::Complete() const
{
    std::cout << "TeleportAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TeleportAction::Step()
{
    std::cout << "TeleportAction::Step() not implemented yet. Doing nothing.\n";
}

void TeleportAction::Stop()
{
    std::cout << "TeleportAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
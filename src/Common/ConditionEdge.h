/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>
#include <memory>

#include "EdgeEvaluators.h"

namespace OPENSCENARIO {
namespace ConditionEdge {

static std::unique_ptr<EdgeEvaluatorBase> from(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICondition> condition)
{
    if(!condition)
    {
        throw std::runtime_error("Cannot resolve ConditionEdge: Condition not set.");
    }

    const auto conditionEdge = condition->GetConditionEdge();
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_0::ConditionEdge::RISING)
    {
        return std::make_unique<RisingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_0::ConditionEdge::FALLING)
    {
        return std::make_unique<FallingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_0::ConditionEdge::RISING_OR_FALLING)
    {
        return std::make_unique<RisingOrFallingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_0::ConditionEdge::NONE)
    {
        return std::make_unique<NoEdgeEvaluator>();
    }

    return std::make_unique<RisingEdgeEvaluator>(); // workaround, as current lib does not parse conditionEdge properly
    //throw std::runtime_error("Cannot resolve ConditionEdge: Invalid condition edge. This can be caused by a default initialized condition.");
}

} // namespace ConditionEdge
} // namespace OPENSCENARIO

